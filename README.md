
# Custom Metrics with GitLab Auto DevOps

## Summary

Prometheus is one of the GitLab managed apps on a Kubernetes cluster. By default it scrapes metrics from selected pods on the cluster. One of these pods is the Ingress Nginx Controller. The GitLab UI has some default dashboards visualizing activity by deployment environments. Documented [here](https://docs.gitlab.com/ee/user/clusters/applications.html#prometheus). This Prometheus server is not to be confused with the one that is installed with the GitLab Omnibus install to support monitoring a GitLab instance. The captured data sets are totally unrelated and every managed cluster can and eventually should have it's own Prometheus server.

## Custom Metrics

The Prometheus server deployed as a managed app is able to scrape additional pods with custom metrics regardless of whether the pods are placed in other namespace than `gitlab-managed-apps`.

## Use Case

Consider a UX example. You implemented a new feature and you want to monitor how frequent this feature is used in a newly deployed environment over time. As in tracking adoption. Metrics about your app are tremendously useful. It's getting even more powerful when combined with feature flags.

## Dependencies

Two things need to happen in order to achieve this.

1. The application needs to be instrumented and has to expose an endpoint (usually `/metrics`). Instrumenting the app is dependent on the programming language and happens in `code`. This project uses Golang thus the language-specific Prometheus libraries get imported. It's documented [here](https://godoc.org/github.com/prometheus/client_golang/prometheus).
1. The Prometheus server needs to *find* the pod(s) that are instrumented. This is achieved by adding Kubernetes annotations to the pod(s). A change of the configuration of the Prometheus server isn't required.

## Visualization

Metrics are best visualized using dashboards. With GitLab we have generally two options available without installing additional software.

1. Dashboards can be created in a GitLab project. You can access them [here](./-/environments/metrics). The `Add Metric` button allows to create custom ones. These dashboards are geared towards the typical GitLab user.
1. For business users or operators it is eventually more useful to use a tool like [Grafana](https://grafana.com/). Fortunately we already ship Grafana with a GitLab Omnibus install. Although it's meant to monitor the GitLab instance, a custom datasource can be added pointing to the Prometheus server running inside a Kubernetes cluster given the Prometheus listen port (9090) is forwarded outside the cluster via a simple port forward. That of course requires that you have access to the cluster with something like `kubectl`.

## Code in the application

The demo code of the app is written in Golang entirely in one single `main.go` file. A `Dockerfile` is used to build the image.

### Importing the required libraries

We need to import a certain set of libraries as follows:

```golang
import (
 "github.com/prometheus/client_golang/prometheus"
 "github.com/prometheus/client_golang/prometheus/promauto"
 "github.com/prometheus/client_golang/prometheus/promhttp"
)
```

Here we prepare our metric that will get used by creating a vector for time series data, defining a name for the metric and a label to be more specific about it. It's advisable to be somewhat generic with the metric name and use labels for varying cases. By the same time have the name of the metric indicating it's meaningand the type of it. In this case `button_presses` and `_total` in order to provide a clue that it is a counter reflecting the number a user pressed a button. It enables us later to more easily group the data to be displayed on a dashboard.

```golang
var (
 buttonProcessed = promauto.NewCounterVec(
  prometheus.CounterOpts{
   Name: "gronk_app_button_presses_total",
   Help: "The total number of invocations",
  },
  []string{"button"},
 )
)

````

When now an event occurs that we want to record we increment a counter and define the label we want to associate with that data point.

```golang
// ButtonsHandler Handle user buttons
func ButtonsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	buttonProcessed.With(prometheus.Labels{"button": vars["button"]}).Inc()
	...
}
```

## Defining pod annotations

This demo strictly uses GitLab Auto DevOps with absolutey no `gitlab-ci.yaml` file in the repository. We however need to make modifications so that when the application pod get's deployed Prometheus is craping the Kubernetes pod.

GitLab's provided Helm chart can be customized following this [documentation](https://docs.gitlab.com/ee/topics/autodevops/customize.html#customize-values-for-helm-chart). The [values.yaml](https://gitlab.com/gitlab-org/charts/auto-deploy-app/-/blob/master/values.yaml) of that Helm chart provides a clue of what can be defined and what defaults are in place. Here the pod annotations are relevant.

```yaml
prometheus:
  metrics: true
```

In the `.gitlab/auto-deploy-values.yaml` stored in the repository the values can be overridden as follows. We have to enable Prometheus metrics.

```yaml
prometheus:
  metrics: true
```

## Testing whether it works

Of course a GitLab pipeline needs to run in order to check whether everything works. Next we want to have some data in Prometheus.

### Creating some data

In order to create some random data just run some shell script hitting the various endpoints of the application like this:

```sh
#!/bin/bash
APP_URL=http://local-cluster-metrics.gronk.cloud
BUTTONS[0]="a"
BUTTONS[1]="b"
BUTTONS[2]="c"
BUTTONS[2]="d"
echo "Press [CTRL+C] to stop.."
while true
do
    curl -s -L $APP_URL/${BUTTONS[`shuf -i 0-3 -n 1`]} > /dev/null
    sleep 1
done
```

### Testing the pod

The first thing to check is whether the application is actually instrumented. For that head over to the `/metrics` endpoint of the pod in a browser. That's however not straightforward as the Ingress in front of the pod will deny access. You can work around that by setting up a direct port forward to the pod's listen port with a tool like `kubectl` or `K9s` or `Lens`.

The output should include something like that:

```text
# HELP gronk_app_button_presses_total The total number of invocations
# TYPE gronk_app_button_presses_total counter
gronk_app_button_presses_total{button="a"} 11369
gronk_app_button_presses_total{button="b"} 11223
gronk_app_button_presses_total{button="c"} 11032
gronk_app_button_presses_total{button="d"} 11298
```

Each line shows the name of the metric with a label that has been used and the absolute count. Prometheus will scrape that data periodically and stores it.

### Looking at the Prometheus server directly

Again we need to forward a port of a pod so we can get straight into it. Look for a pod named `prometheus-prometheus-server-*`in your Kubernetes cluster and forward port `9090` like you did before. When we hit this endpoint with a browser we will see the Prometheus Web UI. In the upper left field a simple query can be defined. For a start the name of the metric will provide some insight into what the data is going to look like.

![Prometheus home screen](./img/prom1.png)

The interesting part is the output list at the bottom. As in this particular project the application has been deployed multiple times to eventually multiple environments and and is even scaled horizontaly. Obviously we want be able to distinguish between them and we can see that Prometheus helps us with that by adding additional labels beyond our already defined label to the metric and as a result maintains multiple time series data sets. Note that the labels `app` and `release` are both referring to the GitLab environment name.

## About querying

In order to query Prometheus PromQL is used. There are many PromQL tutorials on the internet and I won't cover much stuff here except for some basic things.

### Filtering

Filtering is very straightforward. `gronk_app_button_presses_total{button="a",release="production"}` just does what is to be expected. You are only seeing the counter value for those metrics scraped from the `production` environment where the button `a` was pressed.

### Grouping

Almost everytime you will want to group data sets. Consider a production environment is updated multiple times. Consequently the assigned `instance` label will change over time as a pod might get a new IP address assigned. Similarily Prometheus will have distinct values for the `instance` label when you scale a pod.

Grouping of course requires some aggregation and the most useful in this case will be computing a sum. Combining filtering and grouping is most likely what we want. Here we go, `sum(gronk_app_button_presses_total{release="production"}) by (button,release)` is producing a more useful result set. Those familiar with query languages like SQL will feel at home.

In the Prometheus UI we can now create a graph to go beyond just seeing the current counter values.

![Prometheus graph screen](./img/prom2.png)

The most interesting thing is of course that we now have time being printed on the x-axis and therefore now seeing how the data sets have developed over time.

### Computing rates

Clearly the above graph displys more data but it's pretty useless after all as we only ever see an incresing counter value. What we really want is something like a click rate on the graph and PromQL makes that possible with the built-in `rate` function. This give us the ability to better deal with the time dimension here. When we change the query to something like `sum(rate(gronk_app_button_presses_total{release="production"}[2m])) by (button,release)`

## Viewing metrics in GitLab

Now it's time to bring this query over into GitLab. There a few options to do that. Every project has a default dashboard to which metrics can be added. Additionally custom dashboards can be created.

### Default dashboard

Out of the box every project has the default dashboard. It is displaying deployment related metrics collected from the Kubernetes cluster and from managed apps like Ingress NGINX.  New metrics can be added by clicking `New Metric` on the page. As the metrics on the default dashboard are more *technical* in the sense of response times and error rates, more custom business metrics are not ideally placed on this dashboard given it's typically a different user persona interested in these business metrics.

### Creating custom dashboards

In order to create a custom dashboard a YAML file needs to be added in the `.gitlab/dashboards` folder of the projects git repository. Such a dashboard definition can look like this:

```yaml
---
dashboard: Business metrics
priority: 1
panel_groups:
- group: Business metrics
  priority: 0
  panels:
  - type: line-chart
    title: Click rate per button
    y_label: 'req / 2m'
    metrics:
    - query_range: sum(rate(gronk_app_button_presses_total{release="{{ci_environment_slug}}"}[2m])) by
        (button,release)
      unit: rate
      label: ''
```

Please find additional information about defining such dashboards in the [documentation](https://docs.gitlab.com/ee/operations/metrics/dashboards/).

A PromQL query can be customized by referencing variables available in the GitLab UI. Most likely you want to have the query filtering by the selected deployment environment. Notice the query filter here being `release="{{ci_environment_slug}}"` in order to accomplish this.

![dashboard in GitLab](./img/prom3.gif)

## Appendix

### Reconfiguring Prometheus

### Other options to display metrics



