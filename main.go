package main

import (
	"html/template"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type PageData struct {
	CommitTitle            string
	ProjectPath            string
	PipelineUrl            string
	PipelineIid            string
	ProjectId              string
	ProjectUrl             string
	MergeRequestId         string
	MergeRequestTitle      string
	MergeRequestProjectUrl string
	EnvironmentName        string
	CommitSha              string
	ServerUrl              string
	IsMR                   bool
	Buttons                []string
}

// Index Return the home page
func Index(w http.ResponseWriter, r *http.Request) {
	var data PageData
	data.CommitTitle = os.Getenv("CI_COMMIT_TITLE")
	data.ProjectPath = os.Getenv("CI_PROJECT_PATH")
	data.ProjectUrl = os.Getenv("CI_PROJECT_URL")
	data.PipelineUrl = os.Getenv("CI_PIPELINE_URL")
	data.PipelineIid = os.Getenv("CI_PIPELINE_IID")
	data.ProjectId = os.Getenv("CI_PROJECT_ID")
	data.MergeRequestId = os.Getenv("CI_MERGE_REQUEST_IID")
	data.MergeRequestTitle = os.Getenv("CI_MERGE_REQUEST_TITLE")
	data.MergeRequestProjectUrl = os.Getenv("CI_MERGE_REQUEST_PROJECT_URL")
	data.EnvironmentName = os.Getenv("CI_ENVIRONMENT_NAME")
	data.CommitSha = os.Getenv("CI_COMMIT_SHA")
	data.ServerUrl = os.Getenv("CI_SERVER_URL")
	data.Buttons = []string{"Huey", "Dewey", "Louie"}
	if len(data.MergeRequestTitle) > 0 {
		data.IsMR = true
	}

	tmpl := template.Must(template.ParseFiles("static/index.html"))
	tmpl.Execute(w, data)
}

var (
	buttonProcessed = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "gronk_app_button_presses_total",
			Help: "The total number of invocations",
		},
		[]string{"button"},
	)
)

// ButtonsHandler Handle user buttons
func ButtonsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	buttonProcessed.With(prometheus.Labels{"button": vars["button"]}).Inc()
	log.Println(vars["button"])
	//w.WriteHeader(http.StatusOK)
	http.ServeFile(w, r, "static/"+vars["button"]+".jpeg")
	//fmt.Fprintf(w, "Hello %v\n", vars["button"])
}

// faviconHandler Handle favicon
func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "static/favicon.ico")
}

func main() {

	// Handle request
	r := mux.NewRouter()
	r.HandleFunc("/", Index)
	r.HandleFunc("/favicon.ico", faviconHandler)
	r.HandleFunc("/buttons/{button}", ButtonsHandler)
	http.Handle("/", r)
	// Metrics handler for Prometheus to be able to scrape metrics
	http.Handle("/metrics", promhttp.Handler())

	// serve the app
	log.Printf("Server up on port 5000")
	log.Fatal(http.ListenAndServe(":5000", nil))
}
